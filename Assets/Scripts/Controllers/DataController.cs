﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataController : MonoBehaviour {
    
    public class UserData
    {
        private int id;
        public int ID
        {
            get
            { 
                return id;
            }
        }

        private Color assignedColor;
        public Color AssignedColor
        {
            get
            { 
                return assignedColor;
            }
        }

        private int totalWins;
        public int TotalWins
        {
            get
            {
                return totalWins;
            }
        }

        private List<float> roundPoints = new List<float>();
        public List<float> RoundPoints
        {
            get
            { 
                return roundPoints;
            }
        }

        public UserData()
        {
            
        }

        public void SetInfo(Color selectedColor)
        {
            assignedColor = selectedColor;
        }

        public void SetNewRoundPoints(float points)
        {
            roundPoints.Add(points);
        }

        public float GetRoundPoints(int round)
        {
            return roundPoints[round];
        }

        public void NewRound(bool win)
        {
            if (win)
                totalWins++;
        }
    }

    private static DataController instance;
    public static DataController Instance
    {
        get
        {
            return instance;
        }
    }



    private Dictionary<int, UserData> sesionUsers = new Dictionary<int, UserData>();

    private int activeRound;

    public void Awake()
    {
        instance = this;
    }

    public void NewGame()
    {
        activeRound = 0;
    }
        
    public void RegisterNewUser(int userId)
    {
        UserData user = new UserData();
        sesionUsers.Add(userId, user);    
    }

    public void UpdateUserInfo(int userId, Color selectedColor)
    {
        getUser(userId).SetInfo(selectedColor);
        UiController.Instance.UpdatePlayerColors(userId, selectedColor);
    }

    public UserData getUser(int id)
    {
        UserData selectedUser = null;
        if (sesionUsers.TryGetValue(id, out selectedUser))
            Debug.Log(string.Format("User, ID: {0}, was founded in list", id));
        else
            Debug.LogWarning("No User founded with that ID!");

        return selectedUser;
    }

    public UserData getNextUser()
    {
        UserData player;

        if (activeRound % 2 == 0)
        {
            player = getUser(0);
        }
        else
        {
            player = getUser(1);
        }

        activeRound++;
        return player;
    }

    public void SetUserPoints(int id, float points)
    {
        UserData thisUser = null;
        if (sesionUsers.TryGetValue(id, out thisUser))
        {
            thisUser.SetNewRoundPoints(points);
        }
        else
        {
            Debug.LogWarning("No User with this ID was detected");
        }
    }
}
