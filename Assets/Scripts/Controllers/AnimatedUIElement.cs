﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

[System.Serializable]
public class AnimatedUIElement {

	public GameObject	gObject;
	[SerializeField]
	protected Vector2 	inScreenPos;
	protected Vector2 	leftPos;
	protected Vector2 	topPos;
	protected Vector2 	rightPos;
	protected Vector2 	bottomPos;
	protected bool 		initDone = false;

	protected float 		defaultCanvasWidth = 1280;
	protected float 		defaultCanvasHeight = 800;
	protected RectTransform rectT;

	public enum TargetPosition
	{
		RIGHT,
		TOP,
		INSCREEN,
		LEFT,
		BOTTOM
	}

	//-----------------------

    public AnimatedUIElement(Vector2 pos, GameObject gO)
    {
        inScreenPos = pos;
        gObject = gO;
    }

	public virtual void Init(TargetPosition startPos)
	{
		rectT = gObject.GetComponent<RectTransform> ();
		leftPos = getLeftPos (inScreenPos);
		rightPos = getRightPos (inScreenPos);
		topPos = getTopPos (inScreenPos);
		bottomPos = getBottomPos (inScreenPos);
		rectT.anchoredPosition = getAnimationPosition (startPos);
		initDone = true;
	}

	public void Animate(TargetPosition animateTo, float time, float delay)
	{
		//Solucion atada con alambres para adaptarse a logica anterior
		if (!initDone) return;

		Vector2 targetPos = getAnimationPosition (animateTo);

		Hashtable iTweenHash = iTween.Hash (
			                       	"from", rectT.anchoredPosition,
			                       	"to", targetPos,
			                       	"time", time,
			                       	"delay", delay,
			                       	"onupdate", (Action<object>)(newX => rectT.anchoredPosition = (Vector2)newX),
									"easetype",
									"spring"
		);

		iTween.ValueTo (gObject, iTweenHash);
	}

	public void Animate(TargetPosition animateTo, float time, float delay, string effect)
	{
		//Solucion atada con alambres para adaptarse a logica anterior
		if (!initDone) return;
		
		Vector2 targetPos = getAnimationPosition (animateTo);

		Hashtable iTweenHash = iTween.Hash (
			"from", rectT.anchoredPosition,
			"to", targetPos,
			"time", time,
			"delay", delay,
			"onupdate", (Action<object>) (newX => rectT.anchoredPosition = (Vector2) newX),
			"easetype", 
			effect);

		iTween.ValueTo (gObject, iTweenHash);
	}

	public void SetPosition(TargetPosition pos)
	{
		rectT.anchoredPosition = getAnimationPosition (pos);
	}

	protected Vector2 getAnimationPosition(TargetPosition direction)
	{
		switch (direction) {
		case TargetPosition.INSCREEN:
			return inScreenPos;
		case TargetPosition.BOTTOM:
			return bottomPos;
		case TargetPosition.LEFT:
			return leftPos;
		case TargetPosition.RIGHT:
			return rightPos;
		case TargetPosition.TOP:
			return topPos;
		default:
			return Vector2.zero;
		}
	}

	protected Vector2 getLeftPos(Vector2 initialPos)
	{
        return new Vector2 ( -defaultCanvasWidth - rectT.sizeDelta.x, initialPos.y);
	}

	protected Vector2 getRightPos(Vector2 initialPos)
	{
        return new Vector2 (defaultCanvasWidth + rectT.sizeDelta.x, initialPos.y);
	}

	protected Vector2 getTopPos(Vector2 initialPos)
	{
        return new Vector2 (initialPos.x, defaultCanvasHeight + rectT.sizeDelta.y);
	}

	protected Vector2 getBottomPos(Vector2 initialPos)
	{
        return new Vector2 (initialPos.x, -defaultCanvasHeight - rectT.sizeDelta.y);
	}

}

/*
[System.Serializable]
public class UiTitle : AnimatedUIElement
{
	[SerializeField]
	private Text		title;
	[SerializeField]
	private Text		brief;

	public override void Init(TargetPosition startPos)
	{
		rectT = gObject.GetComponent<RectTransform> ();
		leftPos = getLeftPos (centralPos);
		rightPos = getRightPos (centralPos);
		topPos = getTopPos (centralPos);
		bottomPos = getBottomPos (centralPos);
		rectT.anchoredPosition = getAnimationPosition (startPos);
		initDone = true;
	}

	public void SetInfo(string _title, string _brief)
	{
		title.text = _title;
		brief.text = _brief;
	}
}


[System.Serializable]
public class UiDefaultButton : AnimatedUIElement
{
	public delegate void ButtonClick();

	public ButtonClick thisButtonClick;

	public void Init(TargetPosition startPos, ButtonClick buttonDelegate)
	{
		rectT = gObject.GetComponent<RectTransform> ();
		leftPos = getLeftPos (centralPos);
		rightPos = getRightPos (centralPos);
		topPos = getTopPos (centralPos);
		bottomPos = getBottomPos (centralPos);
		rectT.anchoredPosition = getAnimationPosition (startPos);

		thisButtonClick = buttonDelegate;
		gObject.GetComponent<Button>().onClick.AddListener(() => thisButtonClick());

		initDone = true;
	}
}


[System.Serializable]
public class UiMainMenuButton : AnimatedUIElement 
{

	[SerializeField]
	private Text 		title;
	[SerializeField]
	private Text 		brief;

	[HideInInspector]
	public ContentHandler.HomeAIOItemData contenido;

	public void Init(TargetPosition startPos, ContentHandler.HomeAIOItemData data, int pos)
	{
		rectT = gObject.GetComponent<RectTransform> ();
		leftPos = getLeftPos (centralPos);
		rightPos = getRightPos (centralPos);
		topPos = getTopPos (centralPos);
		bottomPos = getBottomPos (centralPos);

		rectT.anchoredPosition = getAnimationPosition (startPos);

		contenido = data;
		title.text = contenido.IdPiso;
		brief.text = contenido.Resumen;
		//Recupera la imagen del menu
		List<Media> listImage = ContentHandler.hashToMediaList(contenido.Media, Media.MediaType.ALL);
		gObject.GetComponent<Image>().sprite = PanelMenuPrincipal.Instance.ImageToSprite(listImage[0]);
		gObject.GetComponent<Button>().onClick.AddListener(() => ButtonClick());
		initDone = true;
	}

	public void ButtonClick()
	{
		if (!PanelMenuPrincipal.Instance.movingButtons) {
		
			PanelMenuPrincipal.Instance.Visible = false;
			PanelInfoAIO.Instance.setearTitulo(contenido.Tema, contenido.Resumen);
			PanelInfoAIO.Instance.panelInfoGenerador.GenerarBloques(contenido.Id);
			PanelInfoAIO.Instance.Visible = true;
		}
	}
}
*/
