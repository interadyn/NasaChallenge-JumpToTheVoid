﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UiController : MonoBehaviour {

    [System.Serializable]
    public class UIElement
    {
        [SerializeField]
        private RectTransform buttonTr;
        public RectTransform ButtonTr
        {
            get
            { 
                return buttonTr;
            }
        }

        private AnimatedUIElement animatedElement;
        public AnimatedUIElement AnimatedElement
        {
            get
            { 
                return animatedElement;
            }
        }

        public void Init(AnimatedUIElement.TargetPosition startPos)
        {
            animatedElement = new AnimatedUIElement(buttonTr.anchoredPosition, buttonTr.gameObject);
            animatedElement.Init(startPos);
        }
    }

    [SerializeField]
    private List<Color> colorDataBase = new List<Color>();
    private List<Color> availableColors;

    [SerializeField]
    private UIElement newGamePlanel;

    [SerializeField]
    private UIElement amountOfUsersPanel;

    [SerializeField]
    private UIElement gamePanel;

    [SerializeField]
    private Text title;

    [SerializeField]
    private Image player1Image;

    [SerializeField]
    private Image player2Image;

    [SerializeField]
    private Text roundNumber;

    [SerializeField]
    private Text player1Points;
    [SerializeField]
    private Text player2Points;

    int amountOfPlayers = 0;

    private static UiController instance;
    public static UiController Instance
    {
        get
        { 
            return instance;
        }
    }

	// Use this for initialization
	void Start () {

        instance = this;

        newGamePlanel.Init(AnimatedUIElement.TargetPosition.INSCREEN);
        amountOfUsersPanel.Init(AnimatedUIElement.TargetPosition.RIGHT);
        gamePanel.Init(AnimatedUIElement.TargetPosition.TOP);

        availableColors = colorDataBase;
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            NewGame();
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            SelectJumper();
        }
    }

    //NUEVO JUEGO BOTON
    public void NewGame()
    {
        newGamePlanel.AnimatedElement.Animate(AnimatedUIElement.TargetPosition.LEFT, .8f, 0);
        amountOfUsersPanel.AnimatedElement.Animate(AnimatedUIElement.TargetPosition.INSCREEN, .8f, 0);
        title.text = "SELECT MODE";
        SoundManager.PlaySFX("Button");
    }

    //BOTON SELECT JUMPER
    public void SelectJumper()
    {
        SoundManager.PlaySFX("Button");
        GameController.instance.StartNewGame(GameMode.Jumper);
        amountOfUsersPanel.AnimatedElement.Animate(AnimatedUIElement.TargetPosition.BOTTOM, .8f, 0);
        gamePanel.AnimatedElement.Animate(AnimatedUIElement.TargetPosition.INSCREEN, .8f, 0);
    }

    public void SelectBallThrow()
    {
        //TODO: ALL ball throw logic 
    }

    public Color GetRandomColorFromDB()
    {
        Color newColor;

        newColor = availableColors[Random.Range(0, availableColors.Count)];
        availableColors.Remove(newColor);

        return newColor;
    }

    [SerializeField]
    private Text animText;
    public void AnimateText(string text)
    {
        animText.text = text;
        animText.GetComponent<Animator>().SetTrigger("GoIn");
    }

    public void UpdatePlayerColors(int id, Color thisColor)
    {
        if (id == 0)
        {
            player1Image.color = thisColor;
        }
        else
        {
            player2Image.color = thisColor;
        }
    }

    public void UpdateRoundNumber(int num)
    {
        roundNumber.text = num.ToString();
    }

    public void UpdatePlayerPoints(int player, int points)
    {
        if (player == 0)
        {
            player1Points.text = points.ToString();
        }
        else
        {
            player2Points.text = points.ToString();
        }
    }
}
