﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public class CircleAnimation
    {
        private bool doAnimation = false;
        public bool DoAnimation
        {
            get
            { 
                return doAnimation;
            }
        }

        private RectTransform tr;
        private Vector2 initDeltaSize;
        private float journeyLength;
        private float startTime;
        private bool gameCircle;
        private Vector2 targetDeltaSize;
        private Vector2 startDeltaSize;
        private float timeToComplete;
        private AnimationReturn animationCallback;

        private float t = 0;
        private float duration;

        public CircleAnimation(RectTransform targetRT, Vector2 targetDS, float timer, bool thisGameCircle, AnimationReturn callback)
        {
            tr = targetRT;
            targetDeltaSize = targetDS;
            startDeltaSize = targetRT.sizeDelta;
            timeToComplete = timer;
            gameCircle = thisGameCircle;
            animationCallback = callback;

            journeyLength = targetRT.sizeDelta.x - targetDS.x;
        
            duration = timer;
        }

        public void StartAnimation()
        {
            doAnimation = true;
            startTime = Time.time;
        }

        private int soundCounter = 0;
        private float soundTimer;
        public void UpdateAnimation()
        {
            if (gameCircle)
            {
                soundTimer += Time.deltaTime;

                if (soundTimer > .8f)
                {
                    soundTimer = 0;
                    SoundManager.PlaySFX("Down", false, 0, 1, 1 - (.1f * soundCounter));
                    soundCounter++;
                }
            }

            tr.sizeDelta = Vector2.Lerp(startDeltaSize, targetDeltaSize, t);
            if (t < 1)
            {
                t += Time.deltaTime / duration;
            }
            else
            {
                tr.sizeDelta = targetDeltaSize;

                if (!gameCircle)
                {
                    tr.gameObject.SetActive(false);
                }

                StopAnimation(true);
            }
        }

        public void StopAnimation(bool ended = false)
        {
            doAnimation = false;

            Debug.Log("AnimationStopped");

            Debug.Log(gameCircle);
            if (gameCircle)
            {
                Debug.Log("StopAnimation tried to call callback");
                int points = (int)(startDeltaSize.x - tr.sizeDelta.x);
                if (ended)
                    points = 0;
                animationCallback(points);    
            }
        }
    }

    public delegate void AnimationReturn(int size);

    private int userId;
    public int UserId
    {
        get
        { 
            return userId;
        }
    }

    private Color thisColor;
    public Color ThisColor
    {
        get
        { 
            return thisColor;
        }
    }

    [SerializeField]
    private RectTransform gameCircle;
    [SerializeField]
    private RectTransform helpCircle;
    [SerializeField]
    private Text targetSpeed;
    [SerializeField]
    private Image thisImage;

    private PlayerReturn gameCallback;

    private CircleAnimation helpCircleAnim;
    private CircleAnimation gameCircleAnim; 
    private float timer;
    private float extraSize;
    private float speedInMtsPerSecond;
    private const float distance = 5;
    private Vector2 gameCircleSize;
    private Vector2 helpCircleSize;

    private void Update()
    {
        if (helpCircleAnim != null)
            if (helpCircleAnim.DoAnimation)
                helpCircleAnim.UpdateAnimation();

        if (gameCircleAnim != null)
            if (gameCircleAnim.DoAnimation)
                gameCircleAnim.UpdateAnimation();
    }

    public void Init(PlayerReturn callback, int id, Color selectedColor)
    {
        gameCallback = callback;
        userId = id;
        thisColor = selectedColor;
        thisImage.color = thisColor;
        StartRound(Random.Range(2.9f, 4.5f));
    }

    public void StartRound(float timeToZero)
    {
        timer = timeToZero;
        speedInMtsPerSecond = 5 / timer; 
        float circleSize = gameCircle.sizeDelta.x;
        float pixelsPerZecond = circleSize / timer;
        float helpCircleSizeVal = pixelsPerZecond + circleSize;
        helpCircleSize = new Vector2(helpCircleSizeVal, helpCircleSizeVal);

        helpCircle.sizeDelta = helpCircleSize;
        targetSpeed.text = string.Format("TARGET SPEED: {0} m/s", speedInMtsPerSecond);

        gameCircleSize = gameCircle.sizeDelta;

        StartCoroutine(CircleAnimationCoroutine());
    }

    private void AnimateHelpCircle()
    {
        helpCircleAnim = new CircleAnimation(helpCircle, gameCircleSize, 1, false, CallBackFromAnimation);
        helpCircleAnim.StartAnimation();

    }

    private void AnimateGameCircle()
    {
        gameCircleAnim = new CircleAnimation(gameCircle, Vector2.zero, timer, true, CallBackFromAnimation);
        gameCircleAnim.StartAnimation();
    }

    private void CallBackFromAnimation(int result)
    {
        SoundManager.PlaySFX("Finish");
        Debug.Log("Callback");
        gameCallback(userId, result, (result == 0));
    }

    private IEnumerator CircleAnimationCoroutine()
    {
        UiController.Instance.AnimateText("3");
        SoundManager.PlaySFX("SecondsSound");
        yield return new WaitForSeconds(1f);
        UiController.Instance.AnimateText("2");
        SoundManager.PlaySFX("SecondsSound");
        yield return new WaitForSeconds(1f);
        UiController.Instance.AnimateText("1");
        AnimateHelpCircle();
        SoundManager.PlaySFX("SecondsSound");
        yield return new WaitForSeconds(1f);
        SoundManager.PlaySFX("Go");
        UiController.Instance.AnimateText("GO!");
        AnimateGameCircle();
    }

    public void Stop()
    {
        if (gameCircleAnim != null)
            if (gameCircleAnim.DoAnimation)
                gameCircleAnim.StopAnimation();
    }

    public void Destroy()
    {
        Destroy(this.gameObject);
    }
}
